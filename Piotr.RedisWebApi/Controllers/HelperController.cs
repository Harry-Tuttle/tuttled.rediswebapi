﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Piotr.RedisWebApi.Data;
using Piotr.RedisWebApi.Models;

namespace Piotr.RedisWebApi.Controllers
{
    public class HelperController : ApiController
    {
        public ICustomerRepository CustomerRepository { get; set; }
        public IOrderRepository OrderRepository { get; set; }

        [HttpGet]
        public string Seed()
        {
            var customerCount = CustomerRepository.GetAll().Count;
            if (customerCount > 0)
                return string.Format("Redis has already been seeded with {0} customers.", customerCount);

            var jane = new Customer()
            {
                Address = new Address()
                {
                    City = "Bethesda",
                    Line1 = "9293 Some Street"
                },
                Id = Guid.NewGuid(),
                Name = "Jane Doe"
            };
            var john = new Customer()
            {
                Address = new Address()
                {
                    City = "Paris",
                    Line1 = "232, Boulevard Awesome"
                },
                Id = Guid.NewGuid(),
                Name = "John Smith"
            };

            var mrMan = new Customer()
            {
                Address = new Address()
                {
                    City = "Miami",
                    Line1 = "2389 Some Street"
                },
                Id = Guid.NewGuid(),
                Name = "Mister Man"
            };

            CustomerRepository.Store(jane);
            CustomerRepository.Store(john);
            CustomerRepository.Store(mrMan);

            var janesOrder = new Order()
            {
                Id = Guid.NewGuid(),
                Lines = new List<OrderLine>()
                                                 {
                                                     new OrderLine()
                                                         {
                                                             Item = "Stapler",
                                                             Quantity = 2,
                                                             TotalAmount = 20m,
                                                         },
                                                     new OrderLine()
                                                         {
                                                             Item = "Candy",
                                                             Quantity = 10,
                                                             TotalAmount = 5m,
                                                         }

                                                 },
            };
            var mrMansOrder = new Order()
            {
                Id = Guid.NewGuid(),
                Lines = new List<OrderLine>()
                                                  {
                                                      new OrderLine()
                                                          {
                                                              Item = "Stapler",
                                                              Quantity = 12,
                                                              TotalAmount = 102.20m,
                                                          },
                                                      new OrderLine()
                                                          {
                                                              Item = "Pitchfork",
                                                              Quantity = 1,
                                                              TotalAmount = 50m,
                                                          }

                                                  },
            };

            var mrMansOrder2 = new Order()
            {
                Id = Guid.NewGuid(),
                Lines = new List<OrderLine>()
                                                   {
                                                       new OrderLine()
                                                           {
                                                               Item = "Book",
                                                               Quantity = 1,
                                                               TotalAmount = 20m,
                                                           },
                                                   },
            };

            OrderRepository.Store(jane, janesOrder);
            OrderRepository.StoreAll(mrMan, new List<Order>() { mrMansOrder, mrMansOrder2 });

            return string.Format("Success!  Redis has been seeded with {0} customers, each with one or more orders.  The popularity of each item ordered has also been stored.", CustomerRepository.GetAll().Count);
        }
    }
}