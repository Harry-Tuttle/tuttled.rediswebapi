﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Piotr.RedisWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "OrdersRoute",
                routeTemplate: "Orders/{action}/{id}/{count}",
                defaults: new
                {
                    controller = "orders",
                    id = RouteParameter.Optional,
                    count = 3,
                }
            );

            config.Routes.MapHttpRoute(
                name: "HelperRoute",
                routeTemplate: "Helper/{action}",
                defaults: new
                {
                    controller = "helper"
                }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
